#include <iostream>
using RetVal = unsigned long long;

/**
 * @nonRecursiveSum - Calculate a fibonacci style series starting from 2.
 * The 15th element of the sequent is: 1220 
 * The implementation starts off by assuming the first two initial values are 2.
 * Therefore the time complexity is: O(n-2) and the space complexity of O(1)
 * cause the used memory doesn't depend upon the function parameter in this case
 * @n, it is constant.
 * The coding principles I followed is: KISS (Keep It Simple and Stupid) cause
 * simplistic codes are easy to read, understand and test. More importantly, it
 * makes sure that it is working and when you have a working code, then it is
 * easier to comprehend in an iterative manner if necessary. Also, while extending
 * test cases can make sure that newly added codes are not breaking the main part
 * of the code.
 */
RetVal nonRecursiveSum(size_t n) {
	RetVal result = 0, curVal = 2, prevVal = 2;

	// Handling corner cases
	if (n == 0)
		return result;

	// 1st and 2nd element are 2 and 2 respectively
	if (n <= 2)
		return curVal;

	for (int index = 0; index < n-2; ++index) {
		result = curVal + prevVal;
		prevVal = curVal;
		curVal = result;
	}

	return result;
}

/**
 * @recursiveSum - Recursively calculate a fibonacci style series starting from 2.
 * In a general manner of speaking the time complexity of this code is O(2^n), here
 * n depends upon the parameter @n and due to it's splitup into two different calls
 * it turned out O(2^n). The space complexity of the function is O(nm), where n is
 * the depth of the recursion tree and m is the space each function takes.
 */
RetVal recursiveSum(size_t n) {
	if (n == 0)
		return 0;
	if (n == 2 || n == 1)
		return 2;
	else
		return recursiveSum(n-1) + recursiveSum(n-2);
}
